// in_out.cpp

#include <stdio.h>
#include <boost/python.hpp>

float sumvals(int array_length, boost::python::numeric::array in) {
  float value = 0.0f;
  for (unsigned int i=0; i< array_length; i++) value += boost::python::extract<float>(in[i]);
  return value;
}

BOOST_PYTHON_MODULE(in_out)
{
    boost::python::numeric::array::set_module_and_type("numpy", "ndarray");
    def("sumvals", sumvals);
}

